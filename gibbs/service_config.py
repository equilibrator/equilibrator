# The MIT License (MIT)
#
# Copyright (c) 2020 Weizmann Institute of Science
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

from matching import approximate_matcher, query_parser, reaction_matcher
from util import singleton


@singleton.Singleton
class ServiceConfig(object):
    """
    A singleton class that contains global service configuration and state.

    All state/config is considered immutable.
        query_parser: parses search queries.
        search_matcher: provides search results and completions.
        reaction_matcher: matches reactions against the database.
    """

    def __init__(self):
        self._query_parser = query_parser.QueryParser()
        self._search_matcher = approximate_matcher.CascadingMatcher(
            max_results=10, min_score=0.1, match_enzymes=True, return_fast=False
        )

        # Don't want to match enzymes when matching reactions.
        self._single_compound_matcher = approximate_matcher.CascadingMatcher(
            max_results=1, min_score=0.1, match_enzymes=False, return_fast=True
        )
        self._reaction_matcher = reaction_matcher.ReactionMatcher(
            self._single_compound_matcher
        )

    query_parser = property(lambda self: self._query_parser)
    search_matcher = property(lambda self: self._search_matcher)
    reaction_matcher = property(lambda self: self._reaction_matcher)


def Get():
    """
    Convenience method to get the single ServiceConfig instance.
    """
    return ServiceConfig()  # Singleton decorator ensures there's only one
