# The MIT License (MIT)
#
# Copyright (c) 2020 Weizmann Institute of Science
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
import base64
import json
import logging
import os

import numpy as np
import bokeh.plotting
import bokeh.embed
import bokeh.themes
from django.apps import apps
from django.http import (
    Http404,
    HttpRequest,
    HttpResponse,
    HttpResponseBadRequest,
)
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from equilibrator_api import Q_, ComponentContribution

from equilibrator.settings import STATIC_ROOT
from gibbs import service_config
from gibbs.conditions import AqueousParams
from gibbs.forms import (
    CompoundForm,
    EnzymeForm,
    ReactionForm,
    ReactionGraphForm,
    SearchForm,
    SuggestForm,
)
from matching.query_parser import ParseError
from util.constants import PH_RANGE_VALUES

from . import _ccache, _predictor

NO_STRUCTURE_THUMBNAIL = os.path.join(
    STATIC_ROOT, "images", "structure_not_available.png"
)


def index(request: HttpRequest) -> HttpResponse:
    return HttpResponse("Hello, world.")


def DownloadPage(request: HttpRequest) -> HttpResponse:
    """Renders the download page."""
    ph_values = list(map(lambda x: f"{x:.1f}", PH_RANGE_VALUES))
    return render(request, "download.html", {"ph_values": ph_values})


def handler404(request: HttpRequest, exception) -> HttpResponse:
    """Renders the error page."""
    return render(request, "404.html", status=404)


def handler429(request: HttpRequest) -> HttpResponse:
    """Renders the error page."""
    return render(request, "429.html")


def handler500(request: HttpRequest) -> HttpResponse:
    """Renders the error page."""
    return render(request, "500.html", status=500)


def handler504(request: HttpRequest) -> HttpResponse:
    """Renders the error page."""
    return render(request, "504.html")


def RefsPage(request: HttpRequest) -> HttpResponse:
    """Renders a list of relevant literature."""
    sources = apps.get_model("gibbs.ValueSource").objects.all()
    sorted_sources = sorted(sources, key=lambda s: s.citation)
    return render(request, "data_refs.html", {"sources": sorted_sources})


def EnzymePage(request: HttpRequest) -> HttpResponse:
    """Renders a page for a particular enzyme."""
    form = EnzymeForm(request.GET)
    if not form.is_valid():
        logging.error(form.errors)
        raise Http404

    enz = apps.get_model("gibbs.Enzyme").objects.get(ec=form.cleaned_ec)
    return render(request, "enzyme_page.html", {"enzyme": enz})


def ResultsPage(request: HttpRequest) -> HttpResponse:
    """Renders the search results page for a given query."""
    form = SearchForm(request.GET)
    if not form.is_valid():
        raise Http404
    logging.debug("Generating a search result page")
    query_parser = service_config.Get().query_parser
    reaction_matcher = service_config.Get().reaction_matcher
    matcher = service_config.Get().search_matcher
    query = form.cleaned_data.get("query", "")
    logging.debug('Query: "%s"', query)
    if not query.strip():
        return render(request, "main.html", {})
    elif query_parser.IsReactionQuery(query):
        # if we should parse and process the input as a reaction.
        logging.info("Parsing the query as a reaction")
        try:
            parsed_reaction = query_parser.ParseReactionQuery(query)
        except ParseError:
            return render(request, "parse_error_page.html")

        reaction_matches = reaction_matcher.MatchReaction(parsed_reaction)
        best_reaction = reaction_matches.GetBestMatch()
        if not best_reaction:
            return render(request, "search_error_page.html", {"query": query})

        logging.debug("Generating a reaction from the matched compounds")
        aq_params = AqueousParams.initialize(form=form, cookies=request.COOKIES)
        rxn = apps.get_model("gibbs.Reaction").FromIds(best_reaction, aq_params)
        return render(
            request, "reaction_page.html", rxn.get_template_data(query)
        )
    else:
        # Otherwise we try to parse it as a single compound.
        logging.debug("Parsing the query as a single compound/enzyme")
        results = matcher.Match(query)

        compound_results = [m for m in results if m.IsCompound()]
        enzyme_results = [m for m in results if m.IsEnzyme()]

        template_data = {
            "compound_results": compound_results,
            "enzyme_results": enzyme_results,
            "enzymes_first": results and results[0].IsEnzyme(),
            "query": query,
        }
        return render(request, "search_results.html", template_data)


def SuggestJson(request: HttpRequest) -> HttpResponse:
    """Renders the suggest JSON."""
    form = SuggestForm(request.GET)
    if not form.is_valid():
        logging.error(form.errors)
        raise Http404
    query = form.cleaned_data.get("query", "")
    suggestions = []
    if query:
        matcher = service_config.Get().search_matcher
        matches = matcher.Match(query)
        suggestions = [m.ToDictForJSON() for m in matches]
    output = {"query": query, "suggestions": suggestions}
    json_data = json.dumps(output)
    return HttpResponse(json_data, content_type="application/json")


@csrf_exempt
def CompoundImage(request: HttpRequest) -> HttpResponse:
    data = request.GET
    compound_id = data.get("compoundId", None)
    if compound_id is None:
        return HttpResponseBadRequest("No request data.")
    compounds = apps.get_model("gibbs.Compound").objects.filter(
        eqapi_id=compound_id
    )
    if not compounds:
        return HttpResponseBadRequest("No such compound.")
    compound = compounds[0]
    image_data = base64.decodebytes(bytes(compound.thumbnail, "ASCII"))
    return HttpResponse(image_data, content_type="image/svg+xml")


def CompoundPage(request: HttpRequest) -> HttpResponse:
    """Renders a page for a particular compound."""
    form = CompoundForm(request.GET)
    if not form.is_valid():
        logging.error(form.errors)
        raise Http404
    aq_params = AqueousParams.initialize(form, request.COOKIES)

    rxn = apps.get_model("gibbs.Reaction").FromForm(form, aq_params)

    reactants = list(rxn.items())

    if len(reactants) == 0:
        return render(request, "parse_error_page.html")

    assert (
            len(reactants) == 1
    ), "Compound page must contain reactions with exactly one reactant."

    eqapi_cpd, coeff = reactants[0]
    assert coeff == 1, "The reactant coefficient in a compound page must be 1"

    # add the "Compound" object to the template_data as well
    compounds = apps.get_model("gibbs.Compound").objects.filter(
        eqapi_id=eqapi_cpd.id
    )
    assert len(compounds) == 1, f"Could not find {eqapi_cpd} in the database"
    compound = compounds[0]

    rxn.compound_names = {eqapi_cpd.id: compound.first_name}
    template_data = rxn.get_template_data(
        query=rxn.get_compound_name(eqapi_cpd)
    )
    template_data.update({"compound": compound})
    response = render(request, "compound_page.html", template_data)
    rxn.aq_params.SetCookies(response)
    return response


_REACTION_TEMPLATES_BY_SUBMIT = {
    "": "reaction_page.html",
    "Update": "reaction_page.html",
    "Reverse": "reaction_page.html",
    "Reset": "reaction_page.html",
}


def ReactionPage(request: HttpRequest) -> HttpResponse:
    """Renders a page for a particular reaction."""
    form = ReactionForm(request.GET)
    if not form.is_valid():
        logging.error(form.errors)
        return HttpResponseBadRequest("Invalid reaction form.")
    aq_params = AqueousParams.initialize(form=form, cookies=request.COOKIES)
    rxn = apps.get_model("gibbs.Reaction").FromForm(form, aq_params)

    if form.cleaned_submit == "Reverse":
        rxn = rxn.Reverse()
    query = rxn.GetQueryString()
    # Render the template.
    if form.cleaned_submit not in _REACTION_TEMPLATES_BY_SUBMIT:
        logging.error(
            "Unknown submit term for reaction page: " + form.cleaned_submit
        )
        raise Http404
    template_name = _REACTION_TEMPLATES_BY_SUBMIT[form.cleaned_submit]
    response = render(request, template_name, rxn.get_template_data(query))
    rxn.aq_params.SetCookies(response)
    return response


def ReactionGraph(request: HttpRequest) -> HttpResponse:
    """Renders the graph page."""
    form = ReactionGraphForm(request.GET)

    if not form.is_valid():
        logging.error(form.errors)
        raise Http404

    aq_params = AqueousParams.initialize(form=form, cookies=request.COOKIES)
    rxn = apps.get_model("gibbs.Reaction").FromForm(form, aq_params)
    comp_contrib = ComponentContribution(ccache=_ccache, predictor=_predictor)
    comp_contrib.p_h = rxn.aq_params.p_h
    comp_contrib.p_mg = rxn.aq_params.p_mg
    comp_contrib.ionic_strength = rxn.aq_params.ionic_strength

    x_data = None
    x_axis_label = None
    y_data = []
    other_conditions = []

    if form.cleaned_vary_ph:
        x_axis_label = "pH"
        x_data = np.linspace(4, 10, 50)
        for p_h in x_data:
            comp_contrib.p_h = Q_(p_h)
            y_data.append(
                comp_contrib.standard_dg_prime(rxn._eqapi_reaction).value.m_as(
                    "kJ/mol"
                ),
            )
    else:
        other_conditions.append(f"pH = {comp_contrib.p_h.m_as(''):.2f}")

    if form.cleaned_vary_pmg:
        x_axis_label = "pMg"
        x_data = np.linspace(0, 5, 50)
        for p_mg in x_data:
            comp_contrib.p_mg = Q_(p_mg)
            y_data.append(
                comp_contrib.standard_dg_prime(rxn._eqapi_reaction).value.m_as(
                    "kJ/mol"
                ),
            )
    else:
        other_conditions.append(f"pMg = {comp_contrib.p_mg.m_as(''):.2f}")

    if form.cleaned_vary_is:
        x_axis_label = "Ionic Strength [M]"
        x_data = np.linspace(0, 0.5, 50)
        for ionic_strength in x_data:
            comp_contrib.ionic_strength = Q_(ionic_strength, "M")
            y_data.append(
                comp_contrib.standard_dg_prime(rxn._eqapi_reaction).value.m_as(
                    "kJ/mol"
                ),
            )
    else:
        other_conditions.append(
            f"I = {comp_contrib.ionic_strength.m_as('M'):.3f} M"
        )

    if not y_data:
        raise ValueError("Internal error: no varying parameter was chosen")

    other_conditions.append(f"T = {comp_contrib.temperature.m_as('K'):.2f} K")

    plot = bokeh.plotting.figure(
        title=None,
        x_axis_label=x_axis_label,
        y_axis_label="ΔG'° [kJ/mol]",
        width=500,
        height=300,
        active_drag=None,
        toolbar_location=None,
    )
    plot.xaxis.axis_label_text_font_size = "10pt"
    plot.yaxis.axis_label_text_font_size = "10pt"
    plot.line(x_data, y_data, line_color="#0066CC", line_width=3)

    script, div = bokeh.embed.components(
        plot, wrap_script=True, wrap_plot_info=True, theme=bokeh.themes.LIGHT_MINIMAL,
    )

    template_data = {
        "reaction": rxn,
        "query": rxn.GetQueryString(),
        "bokeh_script": script,
        "bokeh_div": div,
        "other_conditions": ", ".join(other_conditions),
    }
    template_data.update(rxn.aq_params.get_template_data())
    return render(request, "reaction_graph.html", template_data)
