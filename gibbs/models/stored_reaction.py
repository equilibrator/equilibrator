# The MIT License (MIT)
#
# Copyright (c) 2020 Weizmann Institute of Science
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
import logging

from django.apps import apps
from django.db import models

from .. import eqapi_Compound, eqapi_Reaction
from util import constants


class StoredReaction(models.Model):
    """A reaction stored in the database."""

    # a hash string for fast lookup of enzyme names by reaction
    reaction_hash = models.CharField(max_length=128, db_index=True, unique=True)

    # an (optional) string identifier, e.g. for mapping enzymes to reactions
    accession = models.CharField(max_length=20, null=True)

    # a cache for the __str__ function which can take too long due to DB comm
    reaction_string = models.TextField(null=True)

    # a cache for the Link() function which can take long due to DB comm
    link = models.TextField(null=True)

    @staticmethod
    def get_hyper_link(eqapi_rxn: eqapi_Reaction) -> str:
        params = []
        for phased_compound, coeff in eqapi_rxn.sparse.items():
            params.append(f"reactantsId={phased_compound.id}")
            params.append(f"reactantsCoeff={coeff}")
            name = StoredReaction.compound_to_string(phased_compound)
            params.append(f"reactantsName={name}")
        return f"/reaction?" + "&".join(params)

    @staticmethod
    def get_reaction_string(eqapi_rxn: eqapi_Reaction) -> str:
        return StoredReaction.to_string(eqapi_rxn, use_names=True)

    @staticmethod
    def compound_to_string(eqapi_cpd: eqapi_Compound) -> str:
        compound_model = apps.get_model("gibbs.Compound")
        try:
            compound = compound_model.objects.get(eqapi_id=eqapi_cpd.id)
            return compound.FirstName()
        except compound_model.DoesNotExist:
            logging.warning(f"Cannot find the name for {eqapi_cpd}")
            return f"EQUILIBRATOR:{eqapi_cpd.id:06d}"
        except compound_model.MultipleObjectsReturned as e:
            logging.warning(f"Found multiple compounds with id={eqapi_cpd.id}")
            raise e

    @staticmethod
    def _compound_with_coeff(name: str, coeff: float) -> str:
        if coeff == 1:
            return name
        else:
            return f"{coeff:g} {name}"

    @staticmethod
    def to_string(eqapi_rxn: eqapi_Reaction, use_names: bool = False) -> str:
        """String representation."""
        left = []
        right = []
        for eqapi_cpd, coeff in eqapi_rxn.sparse.items():
            name = (
                StoredReaction.compound_to_string(eqapi_cpd)
                if use_names
                else str(eqapi_cpd.id)
            )
            if coeff < 0:
                left.append(StoredReaction._compound_with_coeff(name, -coeff))
            elif coeff > 0:
                right.append(StoredReaction._compound_with_coeff(name, coeff))
        return f"{' + '.join(left)} => {' + '.join(right)}"
