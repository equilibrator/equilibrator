# The MIT License (MIT)
#
# Copyright (c) 2020 Weizmann Institute of Science
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
from typing import List, Optional, Union

from django import forms
from equilibrator_api import (
    Q_,
    ComponentContribution,
    default_e_potential,
    default_physiological_ionic_strength,
    default_physiological_p_h,
    default_physiological_p_mg,
    default_physiological_temperature,
    ureg,
)


class AqueousParams(object):
    @ureg.check(
        None,
        "",
        "",
        "[concentration]",
        "[temperature]",
        "[energy]/[current]/[time]",  # voltage
    )
    def __init__(
        self,
        p_h: Q_ = default_physiological_p_h,
        p_mg: Q_ = default_physiological_p_mg,
        ionic_strength: Q_ = default_physiological_ionic_strength,
        temperature: Q_ = default_physiological_temperature,
        e_potential: Q_ = default_e_potential,
    ):
        self.p_h = p_h
        self.p_mg = p_mg
        self.ionic_strength = ionic_strength
        self.temperature = temperature
        self.e_potential = e_potential

    @staticmethod
    def initialize(
        form: Optional[forms.Form] = None, cookies: Optional[dict] = None
    ) -> "AqueousParams":
        def _get_parameter(
            key: str,
            default_value: Q_,

        ) -> Union[str, None]:
            value = None
            if form is not None and key in form.cleaned_data:
                value = form.cleaned_data[key]
            elif cookies is not None:
                value = cookies.get(key, None)

            if not value:
                return default_value
            else:
                value = Q_(value)
                if value.unitless and not default_value.unitless:
                    value *= default_value.units
                else:
                    assert value.check(default_value.units), (
                        f"{key} must be in {default_value.units}, "
                        f"but was given in {value.units}"
                    )
                return value

        p_h = _get_parameter("p_h", default_physiological_p_h)
        p_mg = _get_parameter("p_mg", default_physiological_p_mg)
        ionic_strength = _get_parameter(
            "ionic_strength", default_physiological_ionic_strength
        )
        temperature = _get_parameter("temperature", default_physiological_temperature)
        e_potential = _get_parameter("e_potential", default_e_potential)

        return AqueousParams(
            p_h=p_h,
            p_mg=p_mg,
            ionic_strength=ionic_strength,
            temperature=temperature,
            e_potential=e_potential,
        )

    def Clone(self):
        return AqueousParams(
            p_h=self.p_h,
            p_mg=self.p_mg,
            ionic_strength=self.ionic_strength,
            temperature=self.temperature,
            e_potential=self.e_potential,
        )

    @property
    def ionic_strength_in_molar(self) -> float:
        return self.ionic_strength.m_as("molar")

    @property
    def e_potential_in_volt(self) -> float:
        return self.e_potential.m_as("volt")

    def __str__(self):
        return (
            f"pH = {self.p_h}, "
            f"pMg = {self.p_mg}, "
            f"I = {self.ionic_strength}, "
            f"T = {self.temperature}, "
            f"Ψ = {self.e_potential}"
        )

    def set_component_contribution(self, comp_contrib: ComponentContribution) -> None:
        comp_contrib.p_h = self.p_h
        comp_contrib.p_mg = self.p_mg
        comp_contrib.ionic_strength = self.ionic_strength
        comp_contrib.temperature = self.temperature

    def SetCookies(self, response):
        response.set_cookie("p_h", str(self.p_h))
        response.set_cookie("p_mg", str(self.p_mg))
        response.set_cookie("ionic_strength", str(self.ionic_strength))
        response.set_cookie("temperature", str(self.temperature))
        response.set_cookie("e_potential", str(self.e_potential))

    def get_template_data(self):
        return {
            "p_h": str(self.p_h),
            "p_mg": str(self.p_mg),
            "ionic_strength": str(self.ionic_strength),
            "temperature": str(self.temperature),
            "e_potential": str(self.e_potential),
        }

    def get_url_params(self) -> List[str]:
        """
        Get the URL params for this reaction.
        """
        return [
            f"p_h={self.p_h}",
            f"p_mg={self.p_mg}",
            f"ionic_strength={self.ionic_strength}",
            f"temperature={self.temperature}",
            f"e_potential={self.e_potential}",
        ]
