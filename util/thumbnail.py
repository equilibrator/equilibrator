import logging
import subprocess

MOLCONVERT_EXE = 'molconvert'
OBABEL_EXE = "obabel"

class OpenBabelNotFoundError(ImportError):
    def __init__(self):
        super().__init__(
            "obabel was not found on your system. "
            "Please install it from http://openbabel.org/")

class ChemAxonNotFoundError(ImportError):
    def __init__(self):
        super().__init__(
            "ChemAxon molconvert was not found on your system. "
            "Please install it from https://chemaxon.com/")

def run_molconvert(args):
    try:
        inputs = [MOLCONVERT_EXE] + args
        logging.info("INPUT: %s" % ' '.join(inputs))
        result = subprocess.run(inputs,
                                stdout=subprocess.PIPE,
                                stderr=None,
                                universal_newlines=True)
    
        if result.returncode != 0:
            raise RuntimeError(str(args))
    
        logging.debug("OUTPUT: %s" % result.stdout)
        return result.stdout
    
    except OSError:
        raise ChemAxonNotFoundError()

def run_obabel(inchi, args):
    try:
        inputs = [OBABEL_EXE] + args
        logging.debug(f"INPUT: {inchi}")
        result = subprocess.run(inputs,
                                input=inchi,
                                stdout=subprocess.PIPE,
                                stderr=None,
                                universal_newlines=True)

        if result.returncode != 0:
            raise RuntimeError(str(args))

        logging.debug("OUTPUT: %s" % result.stdout)
        return result.stdout
    
    except OSError:
        raise OpenBabelNotFoundError()

def InChI2Thumbnail(
        inchi,
        prog: str = "obabel", 
        output_format: str = "svg",
        size_in_pixels: int = 150
) -> str:
    if prog == "molconvert":
        thumb_string = run_molconvert(
            [output_format + ':w%d,h%d,a' % (size_in_pixels, size_in_pixels),
            '-Y', '-s', inchi+'{inchi}']
        )
    elif prog == "obabel":
        thumb_string = run_obabel(
            inchi, ["-iinchi", f"-o{output_format}",
                    "-xC",  # do not draw terminal C (and attached H) explicitly
                    "-xx",  # omit XML declaration (not displayed in GUI)
                    f"-xP{size_in_pixels}"]
        )
        # see obabel more options in 
        # https://openbabel.org/docs/dev/FileFormats/SVG_2D_depiction.html

    return thumb_string

if __name__ == '__main__':
    inchi = 'InChI=1S/C10H16N5O13P3/c11-8-5-9(13-2-12-8)15(3-14-5)10-7(17)6(16)4(26-10)1-25-30(21,22)28-31(23,24)27-29(18,19)20/h2-4,6-7,10,16-17H,1H2,(H,21,22)(H,23,24)(H2,11,12,13)(H2,18,19,20)/t4-,6-,7-,10-/m1/s1'
    thumb = InChI2Thumbnail(inchi, output_format='svg')
    if thumb is not None:
        fp = open('temp.svg', 'wb')
        fp.write(thumb)
        fp.close()
