eQuilibrator
============
[![pipeline status](https://gitlab.com/elad.noor/equilibrator/badges/master/pipeline.svg)](https://gitlab.com/elad.noor/equilibrator/commits/master)

eQuilibrator is a biochemical thermodynamics calculator website.
The website enables calculation of reaction and compound energies
through an intuitive free-text interface. The current online
version of eQuilibrator can be found at:
http://equilibrator.weizmann.ac.il/

If you are looking for the back-end library used to estimate standard Gibbs free energies
[DOI: 10.1371/journal.pcbi.1003098](http://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1003098),
you can find it in the following GitLab repository:
[eQuilibrator API](https://gitlab.com/elad.noor/equilibrator-api)

eQuilibrator is written in Python using the Django framework.
It was developed primarily on Ubuntu 22.04.3 LTS and is easiest
to develop and set up on that operating system.

The best way to run your own local version of the web server is
using [Docker](https://docs.docker.com/v17.12/install/).

## Running a local web server:
- Make sure that you have `make`, `git` installed (e.g. on Debian/Ubuntu you can use `apt install build-essential git`)
- Make sure you have a recent version of Docker that offers the `compose` command (e.g. follow these [instructions](https://docs.docker.com/compose/install/))
- Clone the repository: `git clone https://gitlab.com/elad.noor/equilibrator`
- Change to the new directory: `cd equilibrator`
- Build the Django database: `make init`
- Now, the docker containers should be ready, and the data is loaded in the DB.

### To start the server:
- Run the command: `make runserver`
- Browse to this [page](http://localhost:8080)

## Running eQuilibrator on AWS
Follow the instructions [here](https://gitlab.com/equilibrator/equilibrator/-/tree/develop/scripts)

