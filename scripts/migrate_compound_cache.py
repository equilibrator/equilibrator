"""A script that copies the compound cache into the PostgreSQL DB."""
# The MIT License (MIT)
#
# Copyright (c) 2013 Weizmann Institute of Science
# Copyright (c) 2020 Institute for Molecular Systems Biology,
# ETH Zurich
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

from equilibrator_cache.zenodo import (
    get_cached_filepath,
    DEFAULT_COMPOUND_CACHE_SETTINGS
)
import logging
import os

db_user = os.environ.get("PGUSER", "eqbtr_user")
db_host = os.environ.get("PGHOST", "localhost")
cpd_db_name = "compounds"

logging.info("Creating the Compound database in PostgreSQL")
cmd = f"createdb -h {db_host} -U {db_user} {cpd_db_name}"
os.system(cmd)

# get the path to the local copy of compounds.sqlite (download it if necessary)
logging.info("Fetching the Compound database from Zenodo (or from the disk cache)")
sqlite_fname = get_cached_filepath(DEFAULT_COMPOUND_CACHE_SETTINGS)

# copy the entire compound cache into the PostgreSQL database using pgloader
logging.info("Loading the Compound database into PostgreSQL")
cmd = f"pgloader sqlite://{sqlite_fname} postgresql://{db_user}@{db_host}/{cpd_db_name}"
os.system(cmd)
