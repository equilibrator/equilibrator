# The MIT License (MIT)
#
# Copyright (c) 2013 Weizmann Institute of Science
# Copyright (c) 2020 Institute for Molecular Systems Biology,
# ETH Zurich
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import argparse
import logging
import os
import django
from django.core.management import execute_from_command_line

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "equilibrator.settings")
django.setup()

logging.getLogger().setLevel(logging.INFO)

from equilibrator.settings import DATABASES
db_name = DATABASES["default"]["NAME"]

############################ MAIN #############################
parser = argparse.ArgumentParser(
    description=('Rebuild eQuilibrator database from SQL dumpfile'))
parser.add_argument('dumpfile', type=str,
                    help='The target dump file')
args = parser.parse_args()

logging.info('> Restoring DB from dump file')
cmd = f"pg_restore --create --clean --dbname={db_name} -Fc {args.dumpfile} 1> /dev/null"
os.system(cmd)

logging.info('> Clearing indices\n')
execute_from_command_line(['', 'clear_index', '--noinput', '-v', 0])

logging.info('> Building indices\n')
execute_from_command_line(['', 'update_index', '-v', 0])

logging.info('> Collecting static files\n')
execute_from_command_line(['', 'collectstatic', '--noinput', '-v', 0])
