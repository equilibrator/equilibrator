## Setting up eQuilibrator on AWS as an EC2 Instance
First, create a new AWS Linux2 instance.
If necessary, download the private key file and save it as `equilibrator_aws.pem`

### Follow these instructions:
- Start an SSH session:
```
ssh -i "equilibrator_aws.pem" ec2-user@ec2-<IP_ADDRESS>.compute-1.amazonaws.com
```
- Install dependencies:
```bash
sudo yum install -y git docker
```
- Start `docker` and add `ec2-user` to the docker group (reboot required):
```
sudo service docker start
sudo usermod -a -G docker ec2-user
sudo chkconfig docker on
sudo reboot
```
- Reconnect again with SSH and install `docker-compose` and `git-lfs`:
```
python3 -m pip install docker-compose git-lfs
```
- Clone the repository and build docker images:
```
git clone -b "<GITLAB_TAG>" --single-branch --depth 1  https://gitlab.com/elad.noor/equilibrator
cd equilibrator
git lfs install
git lfs pull
make init
```

### Before running the Docker stack in production:
- generate a new SECRET_KEY
```python
from django.utils.crypto import get_random_string
chars = 'abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)'
print(get_random_string(50, chars))
```
- In `equilibrator/settings.py` change DEBUG to False, and replace the SECRET_KEY with the one you just generated
- clone [ingress](https://gitlab.com/equilibrator/ingress.git)
- inside the `ingress` directory:
  * create an `.env` configuration file defining `EMAIL=<...>` and `DOMAIN=<...>`
  * run `make setup`
- make the following changes to `ingress/docker-compose.yml`:
  * comment out `--certificatesresolvers.certbot.acme.caserver=https://acme-staging-v02.api.letsencrypt.org/directory`
  * change `--log.level=DEBUG` to `--log.level=WARN`

### Running the Docker stacks:
- Run the command `docker-compose up -d` in `/ingress`
- Run the command `docker-compose up -d` in `/equilibrator`
