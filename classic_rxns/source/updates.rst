What's new ...
==============

in eQuilibrator 3.0?
-------------------------------

* Control of Magnesium ion (pMg).
* Concentrations can be given in nM, uM, mM or M.
* Plots for pH, pMg, and I show in a popup window rather in a new tab.
* More bug fixes.
* Major updates to `equilibrator-api <https://gitlab.com/equilibrator/equilibrator-api>`_ including a `ReadTheDocs <https://equilibrator.readthedocs.io/en/latest/?badge=latest>`_ page.

in eQuilibrator 2.2?
-------------------------------

* `Pathway analysis </pathway>`_ now supports :ref:`Enzyme Cost Minimization <ECM>`.
* Yet another design refresh.
* Long overdue bug fixes.

in eQuilibrator 2.1?
-------------------------------

* The **uncertainty** of each estimation is now provided. For example, when we write Δ\ :sub:`r`\ G'° = -32.1 ± **1.4** [kJ/mol], the 1.4 represents a 95% confidence interval around the estimated Gibbs energy. This change was facilitated by upgrading our software to the more precise Component Contribution method.
* A dedicated page for **reduction potentials** (see for example `Oxaloacetate reduction </search?query=oxaloacetate+%3D+L-malate>`_).
* Gas, liquid and solid phase for the relevant compounds (e.g. you can use O\ :sub:`2`\ (g) and CO\ :sub:`2`\ (g) in `glucose oxidation </search?query=Glucose+%2B+6+O2%28g%29+%3D+6+CO2%28g%29+%2B+6+H2O>`_).
* A fresh design and LOTS of usability upgrades.
* Substantially faster search powered by `Solr <http://lucene.apache.org/solr/>`_.
* Thermodynamic `analysis of metabolic pathways </pathway>`_ using :ref:`Max-min Driving Force <MDF>`.
* A :ref:`new primer <classic-reactions>` on studying classic metabolic reactions using eQuilibrator.
