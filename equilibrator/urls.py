# The MIT License (MIT)
#
# Copyright (c) 2013 Weizmann Institute of Science
# Copyright (c) 2020 Institute for Molecular Systems Biology,
# ETH Zurich
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

"""equilibrator URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from django.views.generic import RedirectView, TemplateView
from django.conf import settings
from django.conf.urls.static import static

from gibbs import views

urlpatterns = [
    path('admin/', admin.site.urls),
#    path('gibbs/', include('gibbs.urls')),
    path('', TemplateView.as_view(template_name='main.html'), name='index'),
    path('compound_image', views.CompoundImage),
    path('compound', RedirectView.as_view(url='/static/compound.html')),
    path('metabolite', views.CompoundPage),
    path('download', views.DownloadPage),
    path('api', RedirectView.as_view(url='https://equilibrator.readthedocs.io/')),
    path('enzyme', views.EnzymePage),
    path('reaction', views.ReactionPage),
    path('graph_reaction', views.ReactionGraph),
    path('data_refs', views.RefsPage),
    path('search', views.ResultsPage),
    path('suggest', views.SuggestJson),
    path('pathway/', include('pathway.urls')),
    path('robots.txt', TemplateView.as_view(template_name='robots.txt')),
    path('404', views.handler404),
    path('429', views.handler429),
    path('500', views.handler500),
    path('504', views.handler504),
    path('timeout', TemplateView.as_view(template_name='pathway_timeout.html'), name='timeout'),
] 

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    
handler404 = views.handler404
handler500 = views.handler500
handler504 = views.handler504
